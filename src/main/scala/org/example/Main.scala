package org.example

import org.apache.spark.sql.SparkSession
import org.example.recordStruct.HiveTableCase
import org.example.recordStruct.RecordStruct.structType

import scala.xml.XML

object Main {
  var HDFS_INPUT = "";
  def main(args: Array[String]): Unit = {

    HDFS_INPUT = args(0).substring(args(0).indexOf("=")+1)
    val sc = SparkSession.builder()
      .appName("xmlparser")
      .master("local")
      .enableHiveSupport()
      .getOrCreate()

    val xmlDf = sc.sqlContext.read
      .format("com.databricks.spark.xml")
      .option("rowTag","rdf:Description")
      .schema(structType)
      .load(HDFS_INPUT)


    import sc.implicits._
    val numsDf = sc.read.table("test_msavelev.nums").as[HiveTableCase]
      .withColumnRenamed("num","odto:nomer_reestrovoy_zapisi")

    xmlDf.join(numsDf,xmlDf.col("odto:nomer_reestrovoy_zapisi") === numsDf.col("odto:nomer_reestrovoy_zapisi"))
      .drop(numsDf.col("odto:nomer_reestrovoy_zapisi"))
      .select("odto:nomer_reestrovoy_zapisi","odto:data_vklyucheniya_svedeniy_v_reestr","odto:data_prinyatiya_resheniya_o_predostavlenii_ili_prekrashchenii_okazaniya_podderzhki",
        "odto:inn", "odto:svedeniya_o_predostavlennoy_podderzhke_forma_podderzhki","odto:svedeniya_o_predostavlennoy_podderzhke_vid_podderzhki",
        "odto:svedeniya_o_predostavlennoy_podderzhke_razmer_podderzhki","odto:svedeniya_o_predostavlennoy_podderzhke_srok_okazaniya_podderzhki")
        .withColumnRenamed("odto:nomer_reestrovoy_zapisi","num")
        .withColumnRenamed("odto:data_vklyucheniya_svedeniy_v_reestr","data_vkl_v_reestr")
        .withColumnRenamed("odto:data_prinyatiya_resheniya_o_predostavlenii_ili_prekrashchenii_okazaniya_podderzhki","data_prin_resheniya")
        .withColumnRenamed("odto:inn","inn")
        .withColumnRenamed("odto:svedeniya_o_predostavlennoy_podderzhke_forma_podderzhki","forma_podderzhki")
        .withColumnRenamed("odto:svedeniya_o_predostavlennoy_podderzhke_vid_podderzhki","vid_podderzhki")
        .withColumnRenamed("odto:svedeniya_o_predostavlennoy_podderzhke_razmer_podderzhki","razmer_podderzhki")
        .withColumnRenamed("odto:svedeniya_o_predostavlennoy_podderzhke_srok_okazaniya_podderzhki","srok_podderzhki")
      .write.insertInto("test_msavelev.reestr")

  }

def parse() = {
  val ns = XML.loadFile("D:\\6SEM\\xmlParser\\src\\main\\scala\\data\\smalldata.xml")
  var list : List[String] = List.empty
  for (n <- ns \ "Description") {
    val at1 = (n \ "nomer_reestrovoy_zapisi").text
    val at2 = (n \ "data_vklyucheniya_svedeniy_v_reestr").text
    val at3 = (n \ "data_prinyatiya_resheniya_o_predostavlenii_ili_prekrashchenii_okazaniya_podderzhki").text
    val at4 = (n \ "inn").text
    val at5 = (n \ "svedeniya_o_predostavlennoy_podderzhke_forma_podderzhki").text
    val at6 = (n \ "svedeniya_o_predostavlennoy_podderzhke_vid_podderzhki").text
    val at7 = (n \ "svedeniya_o_predostavlennoy_podderzhke_razmer_podderzhki").text
    val at8 = (n \ "svedeniya_o_predostavlennoy_podderzhke_srok_okazaniya_podderzhki").text
    }
  }
}
