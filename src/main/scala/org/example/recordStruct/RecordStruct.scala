package org.example.recordStruct

import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}

object RecordStruct {

  val structType: StructType = StructType(
    Seq(
      StructField("odto:nomer_reestrovoy_zapisi", IntegerType),
      StructField("odto:data_vklyucheniya_svedeniy_v_reestr", StringType),
      StructField("odto:data_prinyatiya_resheniya_o_predostavlenii_ili_prekrashchenii_okazaniya_podderzhki", StringType),
      StructField("odto:inn", StringType),
      StructField("odto:svedeniya_o_predostavlennoy_podderzhke_forma_podderzhki", StringType),
      StructField("odto:svedeniya_o_predostavlennoy_podderzhke_vid_podderzhki", StringType),
      StructField("odto:svedeniya_o_predostavlennoy_podderzhke_razmer_podderzhki", StringType),
      StructField("odto:svedeniya_o_predostavlennoy_podderzhke_srok_okazaniya_podderzhki", StringType)
    )
  )
}
case class HiveTableCase(num: Int)