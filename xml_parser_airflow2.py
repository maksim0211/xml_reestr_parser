import os
import sys
import datetime as dt
from airflow import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator


os.environ['SPARK_HOME'] = '/opt/cloudera/parcels/SPARK2-2.3.0.cloudera4-1.cdh5.13.3.p0.611179/lib/spark2'
os.environ['JAVA_HOME'] = '/usr/java/jdk1.8.0_151'
os.environ['HADOOP_USER_NAME'] = 'root'
sys.path.append(os.path.join(os.environ['JAVA_HOME'], 'bin'))
sys.path.append(os.path.join(os.environ['SPARK_HOME'], 'bin'))


default_args = {
    "owner": "airflow",
    "start_date": dt.datetime(2020, 4, 18),
    "retries": 3,
    "retry_delay": dt.timedelta(minutes=1),
    "depends_on_past": False,
}

spark_config = {
    'name': 'xml_parser',
    'application': '/home/airflow/dags/xmlParser-1.0.jar',
    'java_class': 'org.example.Main',
    'jars' : '/home/airflow/dags/spark-xml_2.11-0.4.1.jar',
}

with DAG(
    default_args=default_args,
    dag_id="xml_parser_dag2",
    schedule_interval="*/5 * * * *",
    catchup=False
) as dag:
    spark_operator = SparkSubmitOperator(
        task_id="xml_parser_task",
        application_args=[
	'HDFS_INPUT=hdfs://cld-nmd1.at-consulting.ru:8020/user/root/msavelev/data.xml'],
        **spark_config
    )

spark_operator
